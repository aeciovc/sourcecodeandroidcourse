package br.edu.ac.othersgraphiccomponents.expandablelist;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ExpandableListView;
import br.edu.ac.othersgraphiccomponents.R;

public class ExpandableListActivity extends Activity {
	
	private ExpandableListView mExpandableList;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expandable);
 
 
        mExpandableList = (ExpandableListView)findViewById(R.id.expandable_list);
 
        ArrayList<Parent> arrayParents = new ArrayList<Parent>();
        ArrayList<String> arrayChildren = new ArrayList<String>();
 
        for (int i = 0; i < 10; i++){

            Parent parent = new Parent();
            parent.setTitle("Parent " + i);
             
            arrayChildren = new ArrayList<String>();
            for (int j = 0; j < 5; j++) {
                arrayChildren.add("Child " + j + " from Parent "+i);
            }
            parent.setArrayChildren(arrayChildren);
 
            arrayParents.add(parent);
        }
 
        mExpandableList.setAdapter(new CustomAdapter(ExpandableListActivity.this,arrayParents));
 
    }

}
