package br.edu.ac.cameraphotos;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.Images.Media;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends Activity {

	private static int TAKE_PICTURE = 1;
	private Uri outputFileUri;
	private ImageView imageView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		imageView = (ImageView) findViewById(R.id.imageView);
	}

	public void takePhoto(View v) {
//		getThumbailPicture();
		saveFullImage();
	}

	private void getThumbailPicture() {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		startActivityForResult(intent, TAKE_PICTURE);
	}

	private void saveFullImage() {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		File file = new File(Environment.getExternalStorageDirectory(),
				"teste.jpg");
		outputFileUri = Uri.fromFile(file);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
		startActivityForResult(intent, TAKE_PICTURE);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == TAKE_PICTURE) {
			Uri imageUri = null;
			
			// Check if the result includes a thumbnail Bitmap
			if (data != null) {
				if (data.hasExtra("data")) {
					Bitmap thumbnail = data.getParcelableExtra("data");
					
					imageView.setImageBitmap(thumbnail);
				}else{
					
					Log.i("br.ac.app", "Extra: "+ data.getData().toString());

					String path = data.getData().toString();
					
					File imgFile = new  File(path);
					if(imgFile.exists()){
					    Bitmap photo = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
					    imageView.setImageBitmap(photo);
					}
					
				}
			} else {
				Toast.makeText(this, "Problem on saving", Toast.LENGTH_LONG).show();
			}
		}
	}
}


