package br.ac.alarms;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class AlarmReceiver extends BroadcastReceiver {

	private static final int HELLO_ID = 1;

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub

		int icon = android.R.drawable.ic_input_add;
		CharSequence tickerText = "Hello";
		long when = System.currentTimeMillis();
		CharSequence contentTitle = "My notification";
		CharSequence contentText = "Hello World!";

		PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
				new Intent(), 0);

		Notification notification = new Notification(icon, tickerText, when);
		notification.setLatestEventInfo(context, contentTitle, contentText,
				contentIntent);

		NotificationManager mNotificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);

		mNotificationManager.notify(HELLO_ID, notification);

	}

}
