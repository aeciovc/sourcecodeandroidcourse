package br.ac.alarms;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.widget.Toast;

public class AlarmsActivity extends Activity {
    
	SimpleDateFormat formato;
	
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        formato = new SimpleDateFormat("HH:mm:ss");
    }
    
    public void setAlarm(View v){
    	
    	AlarmManager alarms = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
    	
    	Calendar c = Calendar.getInstance();
    	c.add(Calendar.MINUTE, 1);
    	
    	Date d = new Date();

    	Intent intentToFire = new Intent("EXECUTE_MY_ALARM");
    	PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, intentToFire, PendingIntent.FLAG_ONE_SHOT);
    	
/*    	long onMinuteFromNow =  SystemClock.elapsedRealtime() + 60 * 1000;
    	alarms.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, onMinuteFromNow, pendingIntent);*/
    	
    	alarms.set(AlarmManager.RTC_WAKEUP, c.getTimeInMillis() , pendingIntent);
    	
    	Toast.makeText(this, "Alarm Set", Toast.LENGTH_LONG).show();
    	
    }
    
    public void cancelAlarm(View v){
    	
    	AlarmManager alarms = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

    	Intent intentToFire = new Intent("EXECUTE_MY_ALARM");
    	PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, intentToFire, 0);
    	
    	alarms.cancel(pendingIntent);
    	
    	Toast.makeText(this, "Alarm Canceled", Toast.LENGTH_LONG).show();

    }
    
    public void setRepeatingAlarm(View v){
    
    	Intent intentToFire = new Intent("EXECUTE_MY_ALARM");
    	PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, intentToFire, 0);
    	
    	AlarmManager alarms = (AlarmManager) getSystemService(ALARM_SERVICE);
    	alarms.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, 10*1000,
            AlarmManager.INTERVAL_FIFTEEN_MINUTES, pendingIntent);
    	
    	Toast.makeText(this, "Alarm Set Repeat", Toast.LENGTH_LONG).show();
    }
}