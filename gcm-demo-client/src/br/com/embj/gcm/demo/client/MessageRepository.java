package br.com.embj.gcm.demo.client;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MessageRepository {

	private DatabaseHelper dbHelper;

	public MessageRepository(Context context) {
		dbHelper = new DatabaseHelper(context);
	}

	public void addMessage(String message) {
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		ContentValues values = new ContentValues();

		values.put(DatabaseHelper.KEY_MESSAGE, message);

		db.insert(DatabaseHelper.TABLE_MESSAGE, null, values);
		db.close();
	}

	public void deleteMessage(String message) {
		SQLiteDatabase db = this.dbHelper.getWritableDatabase();
		db.delete(DatabaseHelper.TABLE_MESSAGE,
				DatabaseHelper.KEY_MESSAGE + "=?",
				new String[] { message });
		db.close();
	}

	public List<String> getAllMessages() {
		List<String> messageList = new ArrayList<String>();
		SQLiteDatabase db = this.dbHelper.getReadableDatabase();
		Cursor cursor = db.query(DatabaseHelper.TABLE_MESSAGE, new String[] {DatabaseHelper.KEY_MESSAGE}, null, 
				null, null, null, DatabaseHelper.KEY_ID + " desc");
		if (cursor.moveToFirst()) {
			do {
				messageList.add(cursor.getString(0));
			} while (cursor.moveToNext());
		}
		return messageList;
	}
}

class DatabaseHelper extends SQLiteOpenHelper {
	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = "message_db";
	public static final String TABLE_MESSAGE = "message";
	public static final String KEY_ID = "_id";
	public static final String KEY_MESSAGE = "message";

	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		String CREATE_MESSAGE_TABLE = "CREATE TABLE " + TABLE_MESSAGE + "("
				+ KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," 
				+ KEY_MESSAGE + " TEXT" + ")";
		db.execSQL(CREATE_MESSAGE_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	}
}
