package br.ac.graphiccomponents.edittext;

import br.ac.graphiccomponents.R;
import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class EditTextActivity extends Activity implements OnKeyListener {
	/** Called when the activity is first created. */

	TextView tx;
	EditText edtPassWord;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		loadContentXML();
		// loadContentDynamic();

	}

	private void loadContentXML() {

		setContentView(R.layout.edit_text_view);
		edtPassWord = (EditText) findViewById(R.id.editTextPassword);
		//edtPassWord.setOnKeyListener(this);
		

		// Teclados Virtuais
		edtPassWord.addTextChangedListener(new TextWatcher() {

			public void afterTextChanged(Editable s) {
				Log.i("br.ac.app", "afterTextChanged");
			}

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				Log.i("br.ac.app", "beforeTextChanged");
			}

			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				Log.i("br.ac.app", s.toString());
			}
		});

		tx = (TextView) findViewById(R.id.text1);

	}

	private void loadContentDynamic() {

		LinearLayout layout = new LinearLayout(this);
		setContentView(layout);

		layout.setOrientation(LinearLayout.VERTICAL);

		EditText tv = new EditText(getApplicationContext());
		tv.setText("Type your name");
		layout.addView(tv);

	}

	@Override
	public boolean onKey(View v, int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		Log.i("br.ac.app", "onKey:" + event.getAction());
		if (event.getAction() == KeyEvent.ACTION_UP) {

			if (edtPassWord.getText().length() > 3) {
				tx.setText("Senha Forte");
				Log.i("br.ac.app", "maior que 3");
			} else if (edtPassWord.getText().length() <= 3) {
				tx.setText("Senha Fraca");
				Log.i("br.ac.app", "menor que 3");
			}
		}
		return false;
	}

}