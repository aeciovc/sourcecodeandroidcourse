package br.ac.graphiccomponents.text;

import br.ac.graphiccomponents.R;
import android.app.Activity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

public class TextViewActivity extends Activity {
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		loadContentDynamic();
//		loadContentXML();

	}
	
	private void loadContentXML(){
		
		setContentView(R.layout.text_view);
		TextView t = (TextView) findViewById(R.id.text1);
		
	}
	
	private void loadContentDynamic(){
		
		String[] textArray = { "one", "two", "three" };
		int length = textArray.length;

		LinearLayout layout = new LinearLayout(this);
		setContentView(layout);

		layout.setOrientation(LinearLayout.VERTICAL);
		for (int i = 0; i < length; i++) {
			TextView tv = new TextView(getApplicationContext());
			tv.setText(textArray[i]);
			layout.addView(tv);
		}
	}
}