package br.ac.listviewdetails;

import br.ac.listviewdetails.R;
import android.app.ListActivity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;

public class ListViewActivity extends ListActivity  {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	 
        setListAdapter(ArrayAdapter.createFromResource(getApplicationContext(),
                R.array.titles, R.layout.list_item));
        
        getListView().setOnItemClickListener(new OnItemClickListener() {

       	final String[] links = getResources().getStringArray(R.array.links);
        	
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				String content = links[position];
			    Intent showContent = new Intent(getApplicationContext(),
			            DetailsViewActivity.class);
			    showContent.setData(Uri.parse(content));
			    startActivity(showContent);
			}
		});
    }
}