package br.ac.ownservice;

import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;

public class DownloadService extends IntentService {

	private int result = Activity.RESULT_CANCELED;

	public DownloadService() {
		super("DownloadService");
	}

	// Will be called asynchronously be Android
	@Override
	protected void onHandleIntent(Intent intent) {
		Log.i("br.ac.app", "Inciando Handle...");
		Uri data = intent.getData();
		String urlPath = data.toString();
		String fileName = data.getLastPathSegment();

		Log.i("br.ac.app", "Path Data..." + data.getPath());
		Log.i("br.ac.app", "Last Data..." + data.getLastPathSegment());
		Log.i("br.ac.app", "String Data..." + data.toString());

		DownloadFile downloadFile = new DownloadFile(
				Environment.getExternalStorageDirectory(), fileName, urlPath);
		result = downloadFile.startDownload();

		Bundle extras = intent.getExtras();
		if (extras != null) {
			Messenger messenger = (Messenger) extras.get("MESSENGER");
			Message msg = Message.obtain();
			msg.arg1 = result;
			msg.obj = downloadFile.getPathSaved();
			try {
				Log.i("br.ac.app", "Enviando mensagem ao Handler..");
				messenger.send(msg);
			} catch (android.os.RemoteException e1) {
				Log.w(getClass().getName(), "Exception sending message", e1);
				Log.i("br.ac.app", "Exce��o ao enviar Message....");
			}

		}
	}
}
