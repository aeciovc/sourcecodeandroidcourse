<%@ page import="java.util.List" %>
<%@ page import="br.com.embj.gcm.demo.server.DeviceStore" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
		<link type="text/css" rel="stylesheet" href="css/frame.css"/>
		<title>Usando o Google Cloud Messaging</title>
	</head>
	<body id="corpo">
		<div id="geral">
			
    	  	<div id="topo">
				<div id="titulo" align="center">
					<h1>Usando o Google Cloud Messaging</h1>
				</div>
			</div>
      		
      		<div id="conteudo">
	      		<div id="sub-conteudo">
	      			<form action="sendAll" method="post">
	      				<p><b>Mensangem:</b></p>
	      				<textarea rows="6" name="data" style="width: 90%;"></textarea><br/>
	      				<input type="submit" value="Enviar Mensagem" />
	      				<br/>
	      				<%
      					List<String> devices = DeviceStore.getAllDevices(getServletContext().getRealPath("/"));
	      				if (devices.isEmpty()) {
	      				%>
	      				<h2>Nenhum aparelho resgistrado!</h2>
	      				<%
      					} else {
	      				%>
	      				
	      				<% 
	      					String status = (String) request.getAttribute("status");
	      					if (status != null) {
	      				%>
	      				
	      				<h3><%=status%> </h3>
	      				
	      				<%
      					}
	      				%>
	      				<h2> <%= devices.size() %> aparelho(s) registrado(s)!</h2>
	      				
	      				<table class="gridtable" width="90%">
	                        <tr>
	                            <th width="80%">Registration ID</th>
	                            <th width="20%">Selecionar</th>
	                        </tr>

        				<%
      					for (String regId : devices) {
	      				%>  
		      				<tr>     
			      				<td align="left"><%=regId%></td>                     
			      				<td align="center">  
		                        <input type="checkbox" name="regIds" value="<%=regId%>"/>  
			      				</td>  
		      				</tr>  
	      				<%
      					} // end for 
	      				%> 
	      				</table>
	      				
	      				<%
      					} // end else 
	      				%>
	      			</form>
	      		</div>
			</div>
			
	      	<div id="rodape"></div>
    	</div>
	</body>
</html>