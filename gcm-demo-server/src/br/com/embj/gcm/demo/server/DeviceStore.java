package br.com.embj.gcm.demo.server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public final class DeviceStore {

	
	private DeviceStore() {
		throw new UnsupportedOperationException();
	}

	/**
	 * Registers a device.
	 * @throws IOException 
	 */
	public static void register(String baseDir, String regId) throws IOException {
		List<String> regIds = getAllDevices(baseDir);
		regIds.add(regId);
		saveToFile(baseDir, regIds);
	}
	
	public static List<String> getAllDevices(String baseDir) throws IOException {
		List<String> regIds = new ArrayList<String>();
		File file = new File(baseDir, "devices");
		if (file.exists()) {
			InputStream is = new FileInputStream(file);
			BufferedReader rd = new BufferedReader(new InputStreamReader(is));
			
			String regId = "";
			while ((regId = rd.readLine()) != null) {
				regIds.add(regId);
			}
			
			rd.close();
			is.close();
		}
		return regIds;
	}

	/**
	 * Unregisters a device.
	 * @throws IOException 
	 */
	public static void unregister(String baseDir, String regId) throws IOException {
		List<String> regIds = getAllDevices(baseDir);
		if (regIds.contains(regId)) {
			regIds.remove(regId);
		}
		saveToFile(baseDir, regIds);
	}

	/**
	 * Updates the registration id of a device.
	 */
	public static void updateRegistration(String baseDir, String oldId, String newId) throws IOException {
		List<String> regIds = getAllDevices(baseDir);
		regIds.remove(oldId);
		regIds.add(newId);
		saveToFile(baseDir, regIds);
	}
	
	private static void saveToFile(String baseDir, List<String> regIds) throws IOException {
		File file = new File(baseDir, "devices");
		if (file.exists()) {
			file.delete();
		}
		file.createNewFile();
		
		FileOutputStream fos = new FileOutputStream(file);
		BufferedWriter out = new BufferedWriter(new FileWriter(file, true));
		
		for(String id : regIds) {
			out.write(id + '\n');
		}
		
		out.close();
		fos.close();
	}

}
