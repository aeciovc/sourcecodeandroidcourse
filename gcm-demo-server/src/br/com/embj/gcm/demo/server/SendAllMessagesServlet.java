package br.com.embj.gcm.demo.server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.android.gcm.server.Constants;
import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;

/**
 * Servlet implementation class SendAllMessagesServlet
 */
@SuppressWarnings("serial")
public class SendAllMessagesServlet extends HttpServlet {
	private static final String API_KEY = "AIzaSyCVHq6Uk8XExt9jBvZ8l6FB3JwA1iI-Ons";
	private static final int MULTICAST_SIZE = 1000;
	private String baseDir;
	private Sender sender;

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		sender = new Sender(API_KEY);
	}

	/**
	 * Processes the request to add a new message.
	 */
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException {

		this.baseDir = getServletContext().getRealPath("/");
		String[] selectedIds = req.getParameterValues("regIds");
		String status;
		if (selectedIds == null) {
			status = "Nenhum dispositivo selecionado!";
		} else { 
			String data = req.getParameter("data");
			List<String> devices = Arrays.asList(selectedIds);
			// send a multicast message using JSON
			// must split in chunks of 1000 devices (GCM limit)
			int total = devices.size();
			List<String> partialDevices = new ArrayList<String>(total);
			int counter = 0;
			for (String device : devices) {
				counter++;
				partialDevices.add(device);
				int partialSize = partialDevices.size();
				if (partialSize == MULTICAST_SIZE || counter == total) {
					send(partialDevices, data);
					partialDevices.clear();
				}
			}
			status = "Mensagem enviada com sucesso!";
		}
		req.setAttribute("status", status);
		getServletContext().getRequestDispatcher("/index.jsp").forward(req, resp);
	}

	private void send(List<String> partialDevices, String data) throws IOException {
		// make a copy
		final List<String> devices = new ArrayList<String>(partialDevices);

		Message message = new Message.Builder().addData("data", data).build();
		MulticastResult multicastResult;
		try {
			multicastResult = sender.send(message, devices, 5);
		} catch (IOException e) {
			System.err.println("Error posting messages");
			e.printStackTrace();
			return;
		}
		List<Result> results = multicastResult.getResults();
		// analyze the results
		for (int i = 0; i < devices.size(); i++) {
			String regId = devices.get(i);
			Result result = results.get(i);
			String messageId = result.getMessageId();
			if (messageId != null) {
				System.out.println("Succesfully sent message to device: "
						+ regId + "; messageId = " + messageId);
				String canonicalRegId = result.getCanonicalRegistrationId();
				if (canonicalRegId != null) {
					// same device has more than on registration id:
					// update it
					System.out.println("canonicalRegId " + canonicalRegId);
					DeviceStore.updateRegistration(baseDir, regId, canonicalRegId);
				}
			} else {
				String error = result.getErrorCodeName();
				if (error.equals(Constants.ERROR_NOT_REGISTERED)) {
					// application has been removed from device -
					// unregister it
					System.out.println("Unregistered device: " + regId);
					DeviceStore.unregister(baseDir, regId);
				} else {
					System.out.println("Error sending message to " + regId
							+ ": " + error);
				}
			}
		}
	}

}
