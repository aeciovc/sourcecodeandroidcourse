package br.ac.sharedpreference;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Toast;

public class SharedPreferenceActivity extends Activity implements OnCheckedChangeListener{

	public static final String PREFS_NAME = "SeetingsFile";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
		boolean silent = settings.getBoolean("silentMode", false);

		CheckBox chkSilentMode = (CheckBox) findViewById(R.id.chkSilentMode);
		chkSilentMode.setOnCheckedChangeListener(this);

		if (silent) {
			Toast.makeText(SharedPreferenceActivity.this, "Silent ON",
					Toast.LENGTH_SHORT).show();
			chkSilentMode.setChecked(true);
		} else {
			Toast.makeText(SharedPreferenceActivity.this, "Silent OFF",
					Toast.LENGTH_SHORT).show();
			chkSilentMode.setChecked(false);
		}

	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putBoolean("silentMode", isChecked);
		editor.commit();

		Toast.makeText(SharedPreferenceActivity.this, "Silent Updated", Toast.LENGTH_SHORT).show();
		
	}
}