package br.edu.ac.componentscustomizados.dialog;

import br.edu.ac.componentscustomizados.R;
import br.edu.ac.componentscustomizados.R.drawable;
import br.edu.ac.componentscustomizados.R.id;
import br.edu.ac.componentscustomizados.R.layout;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MyDialog extends Dialog {

	private final Context context;

	private LinearLayout header;
	private LinearLayout content;
	private LinearLayout buttons;

	public MyDialog(Context context) {
		super(context);
		this.context = context;
		initialConfig();
	}

	private void initialConfig() {

		View customView = View.inflate(context, R.layout.my_dialog, null);

		this.header = (LinearLayout) customView
				.findViewById(R.id.linear_layout_dialog_header);

		this.content = (LinearLayout) customView
				.findViewById(R.id.linear_layout_dialog_content);

		this.buttons = (LinearLayout) customView
				.findViewById(R.id.linear_layout_dialog_button_content);

		requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(customView);
/*		
		content.setBackgroundResource(R.drawable.linha_dialog);
		buttons.setBackgroundResource(R.drawable.linha_dialog);*/
		
		getWindow().getDecorView().setBackgroundColor(Color.TRANSPARENT);

	}

	// TITLE
	@Override
	public void setTitle(CharSequence title) {
		TextView txtTitle = (TextView) header.findViewById(R.id.txtDialogTitle);
		txtTitle.setVisibility(View.VISIBLE);
		txtTitle.setText(title);
	}

	@Override
	public void setTitle(int titleId) {
		setTitle(this.context.getString(titleId));
	}

	// BUTTONS
	public void setPositiveButton(int textId, View.OnClickListener listener) {
		setButton((Button) findViewById(R.id.dialogPositiveButton), textId,
				listener);
	}

	public void setNegativeButton(int textId, View.OnClickListener listener) {
		setButton((Button) findViewById(R.id.dialogNegativeButton), textId,
	  			listener);
	}

	private void setButton(Button button, int textId, final View.OnClickListener listener) {
		button.setText(textId);
		button.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				dismiss();
				if (listener != null) {
					listener.onClick(getCurrentFocus());
				}
			}
		});
	}
	
}
