package br.edu.ac.componentscustomizados;

import br.edu.ac.componentscustomizados.dialog.MyDialog;
import br.edu.ac.componentscustomizados.fonts.FontsActivity;
import br.edu.ac.componentscustomizados.listview.ListActivity;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public void showListView(View v){
		Intent intent = new Intent(this, ListActivity.class);
		startActivity(intent);
	}
	
	public void showDialog(View v){
	
		MyDialog dialog = new MyDialog(this);
        dialog.setTitle("Dialog Exemplo");
        dialog.setPositiveButton(R.string.sigin, new OnClickListener() {
            @Override
            public void onClick(View v) {
            	//
            	
            }
        });
        dialog.setNegativeButton(R.string.cancel, new OnClickListener() {
            @Override
            public void onClick(View v) {
            	//
            }
        });
        dialog.show();
		
	}
	
	public void showFonts(View v){
		Intent intent = new Intent(this, FontsActivity.class);
		startActivity(intent);
	}

}
