package br.edu.ac.componentscustomizados.listview;

import java.util.ArrayList;

import br.edu.ac.componentscustomizados.R;
import br.edu.ac.componentscustomizados.R.drawable;
import br.edu.ac.componentscustomizados.R.id;
import br.edu.ac.componentscustomizados.R.layout;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;

public class ListActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.list_activity);
		
		ListView listView = (ListView) findViewById(R.id.list_view);
		
		ArrayList<ItemList> items = new ArrayList<ItemList>();
		items.add(new ItemList("Item 1", R.drawable.facebook));
		items.add(new ItemList("Item 2", R.drawable.twitter));
		items.add(new ItemList("Item 3", R.drawable.whatsapp));
		items.add(new ItemList("Item 4", R.drawable.linkedin));
		
		AdapterListView adapterListView = new AdapterListView(this, items);
		
		listView.setAdapter(adapterListView);
        
	}
	
}
