package br.edu.ac.widget;

import java.util.Random;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.RemoteViews;

public class WidgetProvider extends AppWidgetProvider {

	
    @Override
    public void onEnabled(Context context) {
        super.onEnabled(context);
        Log.i("br.edu.ac.widget", "==> onEnabled");
    }
    
    @Override
    public void onDisabled(Context context) {
        super.onDisabled(context);
        Log.i("br.edu.ac.widget", "==> onDisabled");
    }
    
    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager,
            int[] appWidgetIds) {

        super.onUpdate(context, appWidgetManager, appWidgetIds);
        Log.i("br.edu.ac.widget", "==> onUpdate");

        refreshWidget(context);

    }
    
    private void refreshWidget(Context context) {
    	
        AppWidgetManager manager = AppWidgetManager.getInstance(context);
        ComponentName componentName = new ComponentName(context,
                WidgetProvider.class);
        int[] appWidgetIds = manager.getAppWidgetIds(componentName);
        
        for (int i = 0; i < appWidgetIds.length; ++i) {
        	
        	manager.updateAppWidget(appWidgetIds[i], getView(context, appWidgetIds));
        	manager.notifyAppWidgetViewDataChanged(appWidgetIds[i],
                    R.id.textView1);
        }
    	
    	
    }
    
    
    private RemoteViews getView(Context ctx, int[] appWidgetIds){

        Intent intent = new Intent(ctx, WidgetProvider.class);
        intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds);

        int number = (new Random().nextInt(100));
        
        RemoteViews remoteViews = new RemoteViews(ctx.getPackageName(), R.layout.widget_layout);
        remoteViews.setTextViewText(R.id.textView1, "Novo Valor:" + String.valueOf(number));

        PendingIntent pendingIntent = PendingIntent.getBroadcast(ctx,
                0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            remoteViews.setOnClickPendingIntent(R.id.textView1, pendingIntent);
        
        return remoteViews;

    }
    
    
    @Override
    public void onReceive(Context context, Intent intent) {
    	super.onReceive(context, intent);
    	Log.i("br.edu.ac.widget", "==> onReceive ");
    }
    
    @Override
    public void onDeleted(Context context, int[] appWidgetIds){
        super.onDeleted(context, appWidgetIds);
        Log.i("br.edu.ac.widget", "==> onDeleted ");
    }
	
}
