package br.ac.listview;

import java.util.ArrayList;

import br.ac.listview.R;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class ListViewActivity extends Activity {
    /** Called when the activity is first created. */
	
    ListView listView;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_view);
        
        listView = (ListView) findViewById(R.id.listView1);
        //exemploList1();
        exemploList2();
        
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				// TODO Auto-generated method stub
				String value = (String) parent.getAdapter().getItem(position);
				Toast.makeText(getApplicationContext(),
						"Click ListItem Number " + position + " Value: "+value, Toast.LENGTH_LONG)
						.show();
			}
        	
		}); 

    }
    
    
    private void exemploList1(){
    	
    	String[] values = new String[] { "Android", "iPhone", "WindowsMobile",
    			"Blackberry", "WebOS", "Ubuntu", "Windows7", "Max OS X",
    			"Linux", "OS/2" };
    	
    	// First paramenter - Context
    	// Second parameter - Layout for the row
    	// Third parameter - ID of the View to which the data is written
    	// Forth - the Array of data
    	ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
    		android.R.layout.simple_list_item_1, android.R.id.text1, values);
    	
    	listView.setAdapter(adapter);
    }
    
    private void exemploList2(){
        
    	ArrayList<String> lista = new ArrayList<String>(); 
        lista.add("1 - A�cio");
        lista.add("2 - Jo�o");
        lista.add("3 - Maria");
        lista.add("4 - Juliana");

        listView.setAdapter(new ArrayAdapter(this, R.layout.data_list, lista));
    }
    
}