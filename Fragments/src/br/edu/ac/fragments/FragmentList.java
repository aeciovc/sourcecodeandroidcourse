/**
 * @author A�cio Costa
 */
package br.edu.ac.fragments;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class FragmentList extends Fragment {

	private ItemListener mItemListener;
	private ListView listView;
	
	public interface ItemListener {
        public void onClickItem();
    }
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.list_fragment, container,
				false);
		
		return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		ArrayList<String> listItems = new ArrayList<String>();
		listItems.add("Item 1");
		listItems.add("Item 2");
		listItems.add("Item 3");
		listItems.add("Item 4");
		
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, listItems);
		
		listView = (ListView) getView().findViewById(R.id.listView);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapterView, View view,
					int position, long arg3) {
								
				mItemListener.onClickItem();
			}
		});
		
		
	}
	
	public void setClickItem(ItemListener itemListener){
		this.mItemListener = itemListener;
		
		
	}

	public boolean isInTablet(){
		if (getActivity().getResources().getBoolean(R.bool.isTablet))
			return true;
		else
			return false;
	}
	
}
