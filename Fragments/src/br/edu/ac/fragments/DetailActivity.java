/**
 * @author A�cio Costa
 */
package br.edu.ac.fragments;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

public class DetailActivity extends FragmentActivity {


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detail);
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		
		FragmentDetail fragmentDetail = (FragmentDetail) getSupportFragmentManager().findFragmentById(R.id.fragment_detail);

		if (fragmentDetail != null && fragmentDetail.isInLayout()){
			fragmentDetail.loadData("www.globo.com", "Globo.com");
		}

	}

}
