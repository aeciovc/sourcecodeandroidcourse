/**
 * @author A�cio Costa
 */
package br.edu.ac.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

public class FragmentDetail extends Fragment {

	private WebView webView; 
	private TextView txtTitle;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.detail_fragment, container,
				false);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		webView = (WebView) getView().findViewById(R.id.webViewDetail);
		txtTitle = (TextView) getView().findViewById(R.id.txtTitle);
	}
	
	public void loadData(String url, String text){
		webView.loadUrl(url);
		txtTitle.setText(text);
	}
	
}
