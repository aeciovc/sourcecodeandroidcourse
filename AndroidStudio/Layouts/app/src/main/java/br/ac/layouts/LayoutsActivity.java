package br.ac.layouts;

import br.ac.layouts.R;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class LayoutsActivity extends Activity implements OnItemClickListener {

	private String[] vetor;
	private ListView listView;
	private ArrayAdapter adapter;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		listView = (ListView) this.findViewById(android.R.id.list);
	}

	@Override
	protected void onStart() {
		super.onStart();
		Resources res = this.getResources();
		vetor = new String[3];
		vetor[0] = res.getString(R.string.linear);
		vetor[1] = res.getString(R.string.table);
		vetor[2] = res.getString(R.string.relative);

		listView.setTextFilterEnabled(true);
		
		adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, vetor);
		
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(this);
	}

	public void clicouLinear(View view) {
		Intent i = new Intent(this.getApplicationContext(), LinearDemo.class);
		this.startActivity(i);
	}

	public void clicouTable(View view) {
		Intent i = new Intent(this.getApplicationContext(), TableDemo.class);
		this.startActivity(i);
	}

	public void clicouRelative(View view) {
		Intent i = new Intent(this.getApplicationContext(), RelativeDemo.class);
		this.startActivity(i);
	}

	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		switch (arg2) {
		case 0:
			clicouLinear(null);
			break;
		case 1:
			clicouTable(null);
			break;
		case 2:
			clicouRelative(null);
			break;
		}

	}
}