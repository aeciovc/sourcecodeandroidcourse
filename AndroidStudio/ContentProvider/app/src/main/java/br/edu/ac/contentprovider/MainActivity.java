package br.edu.ac.contentprovider;

import android.app.Activity;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds.StructuredName;
import android.provider.ContactsContract.Data;
import android.provider.ContactsContract.RawContacts;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		ListView list = (ListView) findViewById(R.id.list_view);
		
		ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, android.R.id.text1, ContactsProvider.getContacts(this));
		
		list.setAdapter(arrayAdapter);
		list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int pos,
					long arg3) {
				
				String item = (String) adapterView.getItemAtPosition(pos);
				
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("content://com.android.contacts/contacts/"+ item.substring(0,1) ));
				startActivity(intent);
			}
		
		
		});
		
		list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> adapterView, View view,
					int arg2, long arg3) {

				return false;
			}
		
		
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
	
		
		if (item.getItemId() == R.id.action_insert_contact){
			
			Intent intent = new Intent(Intent.ACTION_INSERT);
			intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
			intent.putExtra(ContactsContract.Intents.Insert.NAME, "some Contact Name");
			intent.putExtra(ContactsContract.Intents.Insert.PHONE, "123-456-7890");

			startActivity(intent);
	
		}else if (item.getItemId() == R.id.action_view_all){
			
//			ContentValues values = new ContentValues();
/*			values.put(ContactsContract.Intents.Insert.NAME, "caio da silva");
			values.put(ContactsContract.Intents.Insert.PHONE, "99900029922");*/
			
			ContentValues values = new ContentValues(); 
	        values.put(RawContacts.ACCOUNT_TYPE, Phone.TYPE_MOBILE); 
	        values.put(RawContacts.ACCOUNT_NAME, "yyyyyyy yssybsybsy"); 
	        Uri rawContactUri = 
	        getContentResolver().insert(RawContacts.CONTENT_URI, values); 
	        long rawContactId = ContentUris.parseId(rawContactUri); 


	        values.clear(); 
	        values.put(Data.RAW_CONTACT_ID, rawContactId); 
	        values.put(Data.MIMETYPE, StructuredName.CONTENT_ITEM_TYPE); 
	        values.put(StructuredName.DISPLAY_NAME, "S.M"); 
	        values.put(Phone.LABEL, "Sarah");
	        values.put(Phone.NUMBER, "91111111");
	        //values.put(ContactsContract.Data.DATA3, "View Contact");
	        getContentResolver().insert(Data.CONTENT_URI, values); 
			
			
			/*values.put(Phone.TYPE, Phone.TYPE_MOBILE);
	        values.put(Phone.NUMBER, "91289161");
			values.put(Phone.DISPLAY_NAME, "Jaslyn");   
	        values.put(Phone.LABEL, "Jaslyn Goh");
			
			getContentResolver().insert(Data.CONTENT_URI, values);*/
			
			Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
			startActivity(intent);
		}
		
		
		return super.onMenuItemSelected(featureId, item);
	}

}
