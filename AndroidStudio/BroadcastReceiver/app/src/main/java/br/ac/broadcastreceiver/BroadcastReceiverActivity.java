package br.ac.broadcastreceiver;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class BroadcastReceiverActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        sendBroadcast(new Intent("EXECUTE_ACTION_TESTE"));
    }
}