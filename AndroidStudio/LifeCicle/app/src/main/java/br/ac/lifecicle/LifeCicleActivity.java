package br.ac.lifecicle;

import ac.calendar.activity.R;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

public class LifeCicleActivity extends Activity {

	private String current_value;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.main);
		
		current_value = "default_value";
		
		Log.i("br.ac.app","onCreate()");
		Log.i("br.ac.app","Value: "+ current_value);
	}

	@Override
	protected void onPause() {
		super.onPause();
		
		current_value = "Algo foi escrito";
		
		Log.i("br.ac.app","onPause()");
		Log.i("br.ac.app","Value Put: "+ current_value);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		Log.i("br.ac.app","onResume()");
		Log.i("br.ac.app","Value Retrieve: "+ current_value);
	}
}
