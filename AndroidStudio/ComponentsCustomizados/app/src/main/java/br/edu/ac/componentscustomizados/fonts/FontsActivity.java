package br.edu.ac.componentscustomizados.fonts;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import br.edu.ac.componentscustomizados.R;

public class FontsActivity extends Activity {
	
	public static final String TYPEFACE_IMPACT_ASSET = "impact.ttf";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fonts_activity);
		
		TextView txtView = (TextView) findViewById(R.id.txtView);
		txtView.setTypeface(getTypeface(TYPEFACE_IMPACT_ASSET));
		
		Button btn = (Button) findViewById(R.id.button1);
		btn.setTypeface(getTypeface(TYPEFACE_IMPACT_ASSET));
		btn.setOnClickListener(null);
		
	}
	
	public Typeface getTypeface(String typefaceAsset) {
		return Typeface.createFromAsset(this.getAssets(),
				typefaceAsset);

	}

}
