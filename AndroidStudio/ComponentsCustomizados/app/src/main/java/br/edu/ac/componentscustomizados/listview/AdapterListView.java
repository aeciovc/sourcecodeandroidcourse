package br.edu.ac.componentscustomizados.listview;

import java.util.List;

import br.edu.ac.componentscustomizados.R;
import br.edu.ac.componentscustomizados.R.id;
import br.edu.ac.componentscustomizados.R.layout;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AdapterListView  extends BaseAdapter {
	
    private List<ItemList> items;
    
    private Context mContext;

    public AdapterListView(Context context, List<ItemList> itens) {
        this.items = itens;
        mContext = context;
    }
    
    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int location) {
        return items.get(location);
    }

    @Override
    public long getItemId(int position) {
        return items.get(position).getIconeRid();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = null;
        
        ItemList item = items.get(position);

        if (convertView == null) {

            LayoutInflater mInflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            
            convertView = mInflater.inflate(R.layout.item_list, null);

            viewHolder = new ViewHolder();
            viewHolder.txtView = (TextView) convertView.findViewById(R.id.text_view);
            viewHolder.imageView = (ImageView)convertView.findViewById(R.id.image_view);

            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.txtView.setText(item.getTexto());
        viewHolder.imageView.setImageResource(item.getIconeRid());
        
        return convertView;
    }
    
    public static class ViewHolder {
        public TextView txtView;
        public ImageView imageView;
    }
}
