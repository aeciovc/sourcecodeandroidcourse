package br.edu.ac.componentscustomizados.listview;

public class ItemList {

	private String texto;
	private int iconId;

	public ItemList() {
		this("", -1);
	}

	public ItemList(String texto, int iconId) {
		this.texto = texto;
		this.iconId = iconId;
	}

	public int getIconeRid() {
		return iconId;
	}

	public void setIconeRid(int iconId) {
		this.iconId = iconId;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

}
