package br.ac.notification;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class NotificationActivity extends Activity implements OnClickListener{
    
	private static final int HELLO_ID = 1;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        Button btnNotfication = (Button) findViewById(R.id.btnNotification);
        btnNotfication.setOnClickListener(this);

    }

	@Override
	public void onClick(View v) {

		int icon = android.R.drawable.ic_input_add;
		CharSequence tickerText = "Hello";
		long when = System.currentTimeMillis();
		CharSequence contentTitle = "My notification";
		CharSequence contentText = "Hello World!";

		PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
				new Intent(), 0);

		Notification notification = new Notification(icon, tickerText, when);
		notification.setLatestEventInfo(this, contentTitle, contentText,
				contentIntent);

		NotificationManager mNotificationManager = (NotificationManager) this
				.getSystemService(Context.NOTIFICATION_SERVICE);

		mNotificationManager.notify(HELLO_ID, notification);

	}
}