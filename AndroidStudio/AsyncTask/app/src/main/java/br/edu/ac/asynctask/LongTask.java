package br.edu.ac.asynctask;

import java.util.ArrayList;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.TextView;

public class LongTask extends AsyncTask<Object, String, String> {
	   
	   private ProgressDialog progress;
	   private Context context;
	   
	   private TextView txtView;

	   public LongTask(Context context) {
	       this.context = context;
	   }
	   @Override
	   protected void onPreExecute() {
	       //Cria novo um ProgressDialog e exibe
	       progress = new ProgressDialog(context);
	       progress.setMessage("Aguarde...");
	       progress.show();
	   }
	   
	   @Override
	   protected String doInBackground(Object... params) {
	       
		   ArrayList<String> list = (ArrayList<String>) params[0];
		   txtView = (TextView) params[1];
		   
		   for (int i = 0; i < list.size(); i++) {
	           try {             
	               
	        	   Thread.sleep(3000); //Simula processo...
	               
	               //Atualiza a interface atrav�s do onProgressUpdate
	               publishProgress("Baixando Conte�do: " +list.get(i) + "...");
	               
	           } catch (Exception e) {}
	       }
	       return "Busca Efetuada";
	    }
	   
	   
	   @Override
	   protected void onPostExecute(String result) {
		   super.onPostExecute(result);
		   txtView.setText(result);
		   progress.dismiss();
	   }
	   
	   @Override
	   protected void onProgressUpdate(String[] values) {
		   super.onProgressUpdate(values);
		   progress.setMessage(values[0]);
	   }; 

	   
}
