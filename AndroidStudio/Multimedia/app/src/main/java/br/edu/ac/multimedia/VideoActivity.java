package br.edu.ac.multimedia;

import android.app.Activity;
import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.VideoView;

public class VideoActivity extends Activity {

	private VideoView mVideoView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_audio);
		
		mVideoView = (VideoView) findViewById(R.id.videoView);
	
	}


	public void play(View v) {
		mVideoView.setVideoPath("//path");
		mVideoView.start();
		mVideoView.requestFocus();
	}
	
	public void stop(View v) {
		mVideoView.stopPlayback();
	}
	
	public void resume(View v) {
		mVideoView.seekTo(mVideoView.getCurrentPosition());
	}
	
	public void pause(View v) {
		mVideoView.pause();
	}

}
