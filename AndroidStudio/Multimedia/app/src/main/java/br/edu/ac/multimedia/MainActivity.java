package br.edu.ac.multimedia;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends Activity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
	}
	
	public void video(View v) {
		startActivity(new Intent(this, VideoActivity.class));
	}
	
	public void audio(View v) {
		startActivity(new Intent(this, AudioActivity.class));
	}
	
	public void record(View v) {
		startActivity(new Intent(this, RecordActivity.class));
	}

}
