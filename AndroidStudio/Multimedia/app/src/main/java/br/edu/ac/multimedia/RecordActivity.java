package br.edu.ac.multimedia;

import java.io.File;
import java.io.IOException;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.Video;
import android.util.Log;
import android.view.View;

public class RecordActivity extends Activity {

	private static int RECORD_VIDEO = 1;
	private static int HIGH_VIDEO_QUALITY = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_record);

	}

	public void recordNative(View v) {
		
		Uri videoUri = Uri.fromFile(new File(Environment.getExternalStorageDirectory(),"fname_" +        
                String.valueOf(System.currentTimeMillis()) + ".3gp"));
		
		Log.i("cesar", "O Path: "+ videoUri.toString());
		
		recordVideo(videoUri);
		
	}

	private void recordVideo(Uri outputpath) {
		Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
		if (outputpath != null)
			intent.putExtra(MediaStore.EXTRA_OUTPUT, outputpath);
		intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, HIGH_VIDEO_QUALITY);
		startActivityForResult(intent, RECORD_VIDEO);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (resultCode == RESULT_OK) {

			if (requestCode == RECORD_VIDEO) {

				Bundle extras = data.getExtras();

				Video v = (Video) extras.get("data");

				
				// Uri recordedVideo = data.getData();
				// TODO Do something with the recorded video
			}
		}
	}

}
