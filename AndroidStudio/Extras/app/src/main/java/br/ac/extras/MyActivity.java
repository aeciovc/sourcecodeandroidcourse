package br.ac.extras;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class MyActivity extends Activity {

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_activity);

		Bundle extras = getIntent().getExtras();

		TextView txt = (TextView) findViewById(R.id.txt);
		txt.setText(extras.getString("chave"));

	}

}
