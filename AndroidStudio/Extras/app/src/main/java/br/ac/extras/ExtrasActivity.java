package br.ac.extras;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class ExtrasActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        String valor = "Ol�, Valor passado como Par�metro";
        
        Intent it = new Intent(this, MyActivity.class);
        it.putExtra("chave", valor);
        
        startActivity(it);

    }
}