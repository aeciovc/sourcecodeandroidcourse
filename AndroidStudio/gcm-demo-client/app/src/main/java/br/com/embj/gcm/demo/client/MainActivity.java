package br.com.embj.gcm.demo.client;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.android.gcm.GCMRegistrar;

public class MainActivity extends Activity {
	ListView mListView;
	MessageRepository mRepository;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        registerReceiver(mHandleMessageReceiver, new IntentFilter(
				GCMIntentService.NEW_MESSAGE_ACTION));
        
        mRepository = new MessageRepository(this);
        
        mListView = (ListView) findViewById(R.id.list);
        mListView.setEmptyView(findViewById(R.id.emptyElement));
        refreshList();
        
        registerForContextMenu(mListView);
        
        final String regId = GCMRegistrar.getRegistrationId(this);
        if (regId.equals("")) {
            // Automatically registers application on startup.
            GCMRegistrar.register(this, ServerUtilities.PROJECT_ID);
        } else {
            // Device is already registered on GCM, check server.
            if (!GCMRegistrar.isRegisteredOnServer(this)) {
            	ServerUtilities.register(this, regId);
            }
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
    		ContextMenuInfo menuInfo) {
    	getMenuInflater().inflate(R.menu.activity_main, menu);
    	super.onCreateContextMenu(menu, v, menuInfo);
    }
    
    @Override
    public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
				.getMenuInfo();

		String selectedMessage = (String) mListView.getItemAtPosition(
				info.position);
		
		switch (item.getItemId()) {
		case R.id.delete:
			mRepository.deleteMessage(selectedMessage);
			refreshList();
			break;
		}
    	return super.onContextItemSelected(item);
    }
    
	protected void onDestroy() {
		try {
			unregisterReceiver(mHandleMessageReceiver);
			GCMRegistrar.onDestroy(this);
		} catch (Exception e) {
			Log.e("UnRegister Receiver Error", "> " + e.getMessage());
		}
        super.onDestroy();
    }

	private void refreshList() {
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				R.layout.text_item, R.id.title, mRepository.getAllMessages());
		mListView.setAdapter(adapter);
	}
	
	private final BroadcastReceiver mHandleMessageReceiver =
            new BroadcastReceiver() {
		
        @Override
        public void onReceive(Context context, Intent intent) {
        	refreshList();
        }
    };

}
