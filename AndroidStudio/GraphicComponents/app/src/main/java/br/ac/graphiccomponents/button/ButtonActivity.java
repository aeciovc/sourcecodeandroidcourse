package br.ac.graphiccomponents.button;

import br.ac.graphiccomponents.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.style.BulletSpan;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

public class ButtonActivity extends Activity implements OnClickListener {
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

//		loadContentDynamic();
		 loadContentXML();
	}

	private void loadContentXML() {

		setContentView(R.layout.button);
		
		Button bt1 = (Button) findViewById(R.id.button1);
		bt1.setOnClickListener(this);
		
		Button bt2 = (Button) findViewById(R.id.button2);
		bt2.setOnClickListener(this);
		
		Button bt3 = (Button) findViewById(R.id.button3);
		bt3.setOnClickListener(this);

	}

	private void loadContentDynamic() {

		LinearLayout layout = new LinearLayout(this);
		setContentView(layout);
		layout.setOrientation(LinearLayout.VERTICAL);

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
		switch (v.getId()) {
		case R.id.button1:
			Log.i("br.ac.app",  "Button 1");
			Toast.makeText(getApplicationContext(), "Bot�o 1", Toast.LENGTH_LONG).show();
			break;
		case R.id.button2:
			Log.i("br.ac.app",  "Button 2");
			Dialog dialog = new Dialog(this);
			dialog.setTitle("Simple Dialog!");
			dialog.show();
			break;
		case R.id.button3:
			Log.i("br.ac.app",  "Button 3");
			AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
            builder2.setTitle("Alerta");
            builder2.setIcon(android.R.drawable.btn_star);
            builder2.setMessage("Meu primeiro Dialog");
            builder2.setPositiveButton(android.R.string.ok,
                            new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                            Toast.makeText(getApplicationContext(),
                                                            "Clicked OK!", Toast.LENGTH_SHORT).show();
                                            return;
                                    }
                            });

            builder2.setNegativeButton(android.R.string.cancel,
                            new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                            Toast.makeText(getApplicationContext(),
                                                            "Clicked Cancel!", Toast.LENGTH_SHORT)
                                                            .show();
                                            return;
                                    }
                            });

            builder2.create();
            builder2.show();
			break;
		default:
			break;
		}
		
		
		
	}
}