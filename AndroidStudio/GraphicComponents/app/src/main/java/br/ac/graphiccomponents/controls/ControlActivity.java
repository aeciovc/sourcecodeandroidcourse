package br.ac.graphiccomponents.controls;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import br.ac.graphiccomponents.R;

public class ControlActivity extends Activity {
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		loadContentXML();

	}

	private void loadContentXML() {

		setContentView(R.layout.controls);
		TextView t = (TextView) findViewById(R.id.text1);

		final RadioGroup rgpSexos = (RadioGroup) findViewById(R.id.sexos);
		
		final CheckBox chkConfig = (CheckBox) findViewById(R.id.chkConfig);
		
		final ImageView image = (ImageView) findViewById(R.id.myImage);
		
		final ImageButton btnImage = (ImageButton) findViewById(R.id.imageButton);
		btnImage.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				btnImage.setImageResource(R.id.myImage);
				
			}
		});

		Button btnChecks = (Button) findViewById(R.id.btnGroup);
		btnChecks.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				if (chkConfig.isChecked()){
					Toast.makeText(getApplicationContext(),
							"Configuração Ativa!", Toast.LENGTH_SHORT).show();
				}

				image.setImageResource(R.drawable.logo);
				
				if (rgpSexos.getCheckedRadioButtonId() == R.id.radioFeminino) {
					Toast.makeText(getApplicationContext(),
							"Feminino Selecionado!", Toast.LENGTH_SHORT).show();
				} else if(rgpSexos.getCheckedRadioButtonId() == R.id.radioMasculino) {
					Toast.makeText(getApplicationContext(),
							"Masculino Selecionado!", Toast.LENGTH_SHORT).show();
				}

			}
		});

	}

}
