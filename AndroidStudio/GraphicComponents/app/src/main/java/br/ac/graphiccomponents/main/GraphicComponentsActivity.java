package br.ac.graphiccomponents.main;

import br.ac.graphiccomponents.R;
import br.ac.graphiccomponents.button.ButtonActivity;
import br.ac.graphiccomponents.controls.ControlActivity;
import br.ac.graphiccomponents.edittext.EditTextActivity;
import br.ac.graphiccomponents.text.TextViewActivity;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class GraphicComponentsActivity extends Activity {
	/** Called when the activity is first created. */
	private static final int TEXT_VIEW = 1;
	private static final int EDIT_VIEW = 2;
	private static final int BUTTONS = 3;
	private static final int CONTROLS = 4;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, TEXT_VIEW, 0, R.string.text_view_menu).setIcon(
				android.R.drawable.ic_menu_add);
		menu.add(0, EDIT_VIEW, 0, R.string.edit_view_menu).setIcon(
				android.R.drawable.ic_media_rew);
		menu.add(0, BUTTONS, 0, R.string.buttons_menu).setIcon(
				android.R.drawable.ic_lock_idle_alarm);
		menu.add(0, CONTROLS, 0, R.string.controls_menu).setIcon(
				android.R.drawable.ic_dialog_dialer);
		return true;
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {

		if (item.getItemId() == TEXT_VIEW) {
			Intent it = new Intent(this, TextViewActivity.class);
			startActivity(it);
			return true;
		} else if (item.getItemId() == EDIT_VIEW) {
			Intent it = new Intent(this, EditTextActivity.class);
			startActivity(it);
			return true;
		} else if (item.getItemId() == BUTTONS) {
			Intent it = new Intent(this, ButtonActivity.class);
			startActivity(it);
			return true;
		} else if (item.getItemId() == CONTROLS) {
			Intent it = new Intent(this, ControlActivity.class);
			startActivity(it);
			return true;
		}
		return false;
	}

}