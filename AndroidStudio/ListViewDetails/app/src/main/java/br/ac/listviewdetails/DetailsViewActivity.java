package br.ac.listviewdetails;

import br.ac.listviewdetails.R;
import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;

public class DetailsViewActivity extends Activity  {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	 setContentView(R.layout.details_view);
    	 
    	 Intent launchingIntent = getIntent();
         String content = launchingIntent.getData().toString();
  
         WebView viewer = (WebView) findViewById(R.id.details_View);
         viewer.loadUrl(content);
    }
}