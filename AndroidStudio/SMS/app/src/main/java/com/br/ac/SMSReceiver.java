package com.br.ac;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;

public class SMSReceiver extends BroadcastReceiver {

	private static final String SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";

	@Override
	public void onReceive(Context context, Intent intent) {
		
		SmsMessage smsMessage = getMessage(intent);
		
		Log.i("br.ac.app", "Mensagem: " + smsMessage.getDisplayMessageBody());
		Log.i("br.ac.app", "De: " + smsMessage.getDisplayOriginatingAddress());

/*		if (intent.getAction().equals(SMS_RECEIVED)) {

			Bundle bundle = intent.getExtras();
			if (bundle != null) {
				Object[] pdus = (Object[]) bundle.get("pdus");
				SmsMessage[] messages = new SmsMessage[pdus.length];
				for (int i = 0; i < pdus.length; i++)
					messages[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
				for (SmsMessage message : messages) {
					String msg = message.getMessageBody();
					String from = message.getOriginatingAddress();
					Log.i(SMSReceiver.class.getName(), "Mensagem: " + msg);
					Log.i(SMSReceiver.class.getName(), "De: " + from);

				}
			}
		}*/
	}

	private SmsMessage[] getMessageFromIntent(Intent intent) {

		Bundle bundle = intent.getExtras();
		if (bundle != null) {
			Object[] pdus = (Object[]) bundle.get("pdus");
			SmsMessage[] messages = new SmsMessage[pdus.length];

			for (int i = 0; i < pdus.length; i++){
				messages[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
				Log.i("br.ac.app", messages[i].getDisplayMessageBody());
			}

			return messages;
		}

		return null;

	}
	
	private SmsMessage getMessage(Intent intent){
		
		SmsMessage[] mensagens = getMessageFromIntent(intent);

		for (int i = 0; i < mensagens.length; i++){
			Log.i("br.ac.app", mensagens[i].getDisplayMessageBody());
		}
		
		if (mensagens != null){
			return mensagens[0];
		}
		
		return null;
		
	}

}
