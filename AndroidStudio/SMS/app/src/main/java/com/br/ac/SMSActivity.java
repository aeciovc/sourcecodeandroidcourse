package com.br.ac;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SMSActivity extends Activity {
	/** Called when the activity is first created. */

	Button btnSendSMS;
	EditText txtPhoneNo;
	EditText txtMessage;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		btnSendSMS = (Button) findViewById(R.id.btnSendSMS);
		txtPhoneNo = (EditText) findViewById(R.id.txtPhoneNo);
		txtMessage = (EditText) findViewById(R.id.txtMessage);

		btnSendSMS.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				String phoneNo = txtPhoneNo.getText().toString();
				String message = txtMessage.getText().toString();
				if (phoneNo.length() > 0 && message.length() > 0)
					sendSMS(phoneNo, message);
				else
					Toast.makeText(getBaseContext(),
							"Please enter both phone number and message.",
							Toast.LENGTH_SHORT).show();
			}

		});

		Button btnMMS = (Button) findViewById(R.id.btnMMS);
		btnMMS.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				sendMMS();

			}
		});

		Button btnSMSNative = (Button) findViewById(R.id.btnSMSNative);
		btnSMSNative.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				sendSMSForAppNative();

			}
		});
	}

	private void sendSMS(String phoneNumber, String message) {

		String SENT = "SENT_SMS_ACTION";
		String DELIVERED = "DELIVERED_SMS_ACTION";

		PendingIntent sentPI = PendingIntent.getBroadcast(SMSActivity.this, 0,
				new Intent(SENT), 0);

		PendingIntent deliveredPI = PendingIntent.getBroadcast(
				SMSActivity.this, 0, new Intent(DELIVERED), 0);

		// ---when the SMS has been sent---
		registerReceiver(new BroadcastReceiver() {
			@Override
			public void onReceive(Context arg0, Intent arg1) {
				switch (getResultCode()) {
				case Activity.RESULT_OK:
					Toast.makeText(SMSActivity.this, "SMS sent",
							Toast.LENGTH_SHORT).show();
					break;
				case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
					Toast.makeText(SMSActivity.this, "Generic failure",
							Toast.LENGTH_SHORT).show();
					break;
				case SmsManager.RESULT_ERROR_NO_SERVICE:
					Toast.makeText(SMSActivity.this, "No service",
							Toast.LENGTH_SHORT).show();
					break;
				case SmsManager.RESULT_ERROR_NULL_PDU:
					Toast.makeText(SMSActivity.this, "Null PDU",
							Toast.LENGTH_SHORT).show();
					break;
				case SmsManager.RESULT_ERROR_RADIO_OFF:
					Toast.makeText(getBaseContext(), "Radio off",
							Toast.LENGTH_SHORT).show();
					break;
				}
			}
		}, new IntentFilter(SENT));

		// ---when the SMS has been delivered---
		registerReceiver(new BroadcastReceiver() {
			@Override
			public void onReceive(Context arg0, Intent arg1) {
				switch (getResultCode()) {
				case Activity.RESULT_OK:
					Toast.makeText(SMSActivity.this, "SMS delivered",
							Toast.LENGTH_SHORT).show();
					break;
				case Activity.RESULT_CANCELED:
					Toast.makeText(SMSActivity.this, "SMS not delivered",
							Toast.LENGTH_SHORT).show();
					break;
				}
			}
		}, new IntentFilter(DELIVERED));

		/*
		 * BroadcastReceiver b1 = new BroadcastReceiver() {
		 * 
		 * @Override public void onReceive(Context context, Intent intent) { //
		 * TODO Auto-generated method stub Toast.makeText(SMSActivity.this,
		 * getResultCode() + " " + Activity.RESULT_OK,
		 * Toast.LENGTH_SHORT).show(); } };
		 */

		// registerReceiver(b1, new IntentFilter(CORRECT_DELIVERY));

		SmsManager sms = SmsManager.getDefault();
		sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
	}

	public void sendSMSForAppNative() {
		Intent smsIntent = new Intent(Intent.ACTION_SENDTO,
				Uri.parse("sms:5556"));
		smsIntent.putExtra("sms_body", "Press send to send me");
		startActivity(smsIntent);

		Log.i("br.ac.app", "Mensagem Enviada");
	}

	public void sendMMS() {

		// Get the URI of a piece of media to attach.
		Uri attached_Uri = Uri.parse("content://media/external/images/media/1");

		// Create a new MMS intent
		Intent mmsIntent = new Intent(Intent.ACTION_SEND, attached_Uri);
		mmsIntent.putExtra("sms_body", "Please see the attached image");
		mmsIntent.putExtra("address", "5556");
		mmsIntent.putExtra(Intent.EXTRA_STREAM, attached_Uri);
		mmsIntent.setType("image/png");
		startActivity(mmsIntent);
	}

}
