package br.ac.ownservice;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class OwnServiceActivity extends Activity {
	private Handler handler = new Handler() {
		public void handleMessage(Message message) {
			Object path = message.obj;
			if (message.arg1 == RESULT_OK && path != null) {
				Log.i("br.ac.app", "Conte�do Baixado..." + path.toString());
				Toast.makeText(OwnServiceActivity.this,
						"Downloaded" + path.toString(), Toast.LENGTH_LONG)
						.show();
			} else {
				Log.i("br.ac.app", "Conte�do N�o Baixado..." + path.toString());
				Toast.makeText(OwnServiceActivity.this, "Download failed.",
						Toast.LENGTH_LONG).show();
			}

		};
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

	}

	public void onClick(View view) {
		Intent intent = new Intent(this, DownloadService.class);
		// Create a new Messenger for the communication back
		Messenger messenger = new Messenger(handler);
		intent.putExtra("MESSENGER", messenger);
		intent.setData(Uri.parse("http://www.aeciocosta.com.br/index.php"));
		startService(intent);
		Log.i("br.ac.app", "Inciando o Servi�o...");
	}
}