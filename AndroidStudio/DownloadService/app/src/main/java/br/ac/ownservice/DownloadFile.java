package br.ac.ownservice;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import android.util.Log;

public class DownloadFile {

	private File path;
	private String fileName;
	private String urlPath;

	private int SUCESS_RESULT = 0;
	private int FAIL_RESULT = 1;

	private int RESULT = FAIL_RESULT;

	private File output;

	public DownloadFile(File path, String fileName, String urlPath) {
		this.path = path;
		this.fileName = fileName;
		this.urlPath = urlPath;
		output = new File(this.path, this.fileName);
	}

	public int startDownload() {

		if (output.exists()) {
			output.delete();
		}

		InputStream stream = null;
		FileOutputStream fos = null;
		try {

			URL url = new URL(this.urlPath);
			stream = url.openConnection().getInputStream();

			InputStreamReader reader = new InputStreamReader(stream);
			fos = new FileOutputStream(output.getPath());

			int next = -1;
			while ((next = reader.read()) != -1) {
				fos.write(next);
			}

			RESULT = SUCESS_RESULT;

		} catch (Exception e) {
			e.printStackTrace();
			Log.i("cesar", "Erro: " + e.getMessage());
		} finally {
			if (stream != null) {
				try {
					stream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (fos != null) {
				try {
					fos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return RESULT;
	}

	public String getPathSaved() {
		return output.getAbsolutePath();
	}

}

/*		File output = new File(Environment.getExternalStorageDirectory(),
fileName);
if (output.exists()) {
output.delete();
}

InputStream stream = null;
FileOutputStream fos = null;
try {

URL url = new URL(urlPath);
stream = url.openConnection().getInputStream();
InputStreamReader reader = new InputStreamReader(stream);
fos = new FileOutputStream(output.getPath());
int next = -1;
while ((next = reader.read()) != -1) {
fos.write(next);
}
// Sucessful finished
result = Activity.RESULT_OK;
Log.i("cesar", "Resultado finalizado Handle...");

} catch (Exception e) {
e.printStackTrace();
Log.i("cesar", "Erro: "+e.getMessage());
} finally {
if (stream != null) {
try {
	stream.close();
} catch (IOException e) {
	e.printStackTrace();
}
}
if (fos != null) {
try {
	fos.close();
} catch (IOException e) {
	e.printStackTrace();
}
}
}*/


