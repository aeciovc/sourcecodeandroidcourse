/**
 * @author A�cio Costa
 */
package br.edu.ac.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

public class MainActivity extends FragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		final FragmentList fragmentList = (FragmentList) getSupportFragmentManager().findFragmentById(R.id.fragment_list);
		
		if (fragmentList != null && fragmentList.isInLayout()){
			
			if (!fragmentList.isInTablet()){
				
				fragmentList.setClickItem(new FragmentList.ItemListener() {
					
					@Override
					public void onClickItem() {
						Intent intent = new Intent(MainActivity.this, DetailActivity.class);
						startActivity(intent);
					}
				});
				
				
			}
		}
		
	}
}
