package br.ac.datetimepicker;

import java.util.Calendar;
import java.util.Date;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.Toast;

public class DateTimePickerActivity extends Activity {

	DatePicker datePicker;
	
    DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                int dayOfMonth) {
            String data = String.valueOf(dayOfMonth) + " /"
                    + String.valueOf(monthOfYear+1) + " /" + String.valueOf(year);
            Toast.makeText(DateTimePickerActivity.this,
                    "DATA = " + data, Toast.LENGTH_SHORT)
                    .show();
        }
    };
    
    DatePicker.OnDateChangedListener mDatePickerSetListener = new DatePicker.OnDateChangedListener() {
		
		@Override
		public void onDateChanged(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			String data = String.valueOf(dayOfMonth) + " /"
                    + String.valueOf(monthOfYear+1) + " /" + String.valueOf(year);
            Toast.makeText(DateTimePickerActivity.this,
                    "DATA = " + data, Toast.LENGTH_SHORT)
                    .show();
			
		}
	};
      

	
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.date_time_picker);
        
        Button btnDialog = (Button) findViewById(R.id.btnDialogPicker);
        btnDialog.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
					showDialog();
			}
		});
        
        final Calendar calendar = Calendar.getInstance();

		TimePicker timePicker = (TimePicker) findViewById(R.id.timePicker1);
		//timePicker.setIs24HourView(true);

		Toast.makeText(this, "Hora TimePicker: "+ timePicker.getCurrentHour(), Toast.LENGTH_LONG);
		
		datePicker = (DatePicker) findViewById(R.id.datePicker1);

		if (calendar != null && calendar.get(Calendar.HOUR_OF_DAY) > 0) {
			timePicker.setCurrentHour(calendar.get(Calendar.HOUR_OF_DAY));
			timePicker.setCurrentMinute(calendar.get(Calendar.MINUTE));
		}

		timePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
			
			@Override
			public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
				// TODO Auto-generated method stub
				Toast.makeText(getApplicationContext(), "Time Changed ", Toast.LENGTH_LONG).show();
			}
		});
		
		datePicker.init(2010, 5, 13, mDatePickerSetListener);
		
		datePicker.updateDate(calendar.get(Calendar.YEAR), 5, 10);

    }
 
   
    public void showDialog(){
    	
    	Calendar calendario = Calendar.getInstance();
    	
    	calendario.setTime(new Date());
        
        int ano = calendario.get(Calendar.YEAR);
        int mes = calendario.get(Calendar.MONTH);
        int dia = calendario.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dialog =  new DatePickerDialog(this, mDateSetListener, ano , mes, dia);
        dialog.show();
    }
    
    public void showFields(View v){
    	Toast.makeText(this, "M�s DatePicker: "+ datePicker.getMonth(), Toast.LENGTH_LONG).show();
		Toast.makeText(this, "Dia DatePicker: "+ datePicker.getDayOfMonth(), Toast.LENGTH_LONG).show();
		Toast.makeText(this, "Ano DatePicker: "+ datePicker.getYear(), Toast.LENGTH_LONG).show();
    }
   
}