package br.ac.alertdialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.Toast;

public class AlertDialogActivity extends Activity {
	
	AlertDialog.Builder builder;
	
    final String[] items = new String[]{"N�utico","Sport", "Santa Cruz"};
    final boolean[] checks = {true,false, false};
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		builder = new AlertDialog.Builder(this);

		exemploMultipleChoice();
//		exemploOneChoice();
//		exemploSimpleDialog();
		
		builder.show();

	}
	
	private void exemploSimpleDialog(){

		builder.setTitle("Alerta");
		builder.setIcon(android.R.drawable.btn_star);
		builder.setMessage("Mensagem");
		
		builder.setPositiveButton(android.R.string.ok,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						Toast.makeText(getApplicationContext(), "Clicked OK!",
								Toast.LENGTH_SHORT).show();
						return;
					}
				});

		builder.setNegativeButton(android.R.string.cancel,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						Toast.makeText(getApplicationContext(),
								"Clicked Cancel!", Toast.LENGTH_SHORT).show();
						return;
					}
				});
		
		// builder.setNeutralButton(android.R.string.ok, listener);
	}
	
	private void exemploMultipleChoice(){
		
        builder.setMultiChoiceItems(items,checks ,new DialogInterface.OnMultiChoiceClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which, boolean isChecked) {
				Toast.makeText(getApplicationContext(), items[which], Toast.LENGTH_SHORT).show();
			}
		}  );
        
	}
	
	private void exemploOneChoice(){
	       
      builder.setItems(items, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				Toast.makeText(getApplicationContext(), items[which], Toast.LENGTH_SHORT).show();
			}

		});
      
	}
}