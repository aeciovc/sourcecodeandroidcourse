package br.edu.ac.othersgraphiccomponents.expandablelist;

import java.util.ArrayList;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.TextView;
import br.edu.ac.othersgraphiccomponents.R;


public class CustomAdapter extends BaseExpandableListAdapter {
 
 
    private LayoutInflater inflater;
    private ArrayList<Parent> mParent;
 
    public CustomAdapter(Context context, ArrayList<Parent> parent){
        mParent = parent;
        inflater = LayoutInflater.from(context);
    }
 
 
    @Override
    public int getGroupCount() {
        return mParent.size();
    }
 
    @Override
    public int getChildrenCount(int groupPosition) {
        return mParent.get(groupPosition).getArrayChildren().size();
    }
 
    @Override
    public Object getGroup(int groupPosition) {
        return mParent.get(groupPosition).getTitle();
    }
 
    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return mParent.get(groupPosition).getArrayChildren().get(childPosition);
    }
 
    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }
 
    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }
 
    @Override
    public boolean hasStableIds() {
        return true;
    }
 
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
 
        ViewHolder holder = new ViewHolder();
        holder.groupPosition = groupPosition;
 
        if (convertView == null) {
        	convertView = inflater.inflate(R.layout.item_parent, parent,false);
        }
 
        TextView textView = (TextView) convertView.findViewById(R.id.list_item_text_view);
        textView.setText(getGroup(groupPosition).toString());
 
 
        convertView.setTag(holder);
 
        return convertView;
    }
 
    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
 
        ViewHolder holder = new ViewHolder();
        holder.childPosition = childPosition;
        holder.groupPosition = groupPosition;
 
        if (convertView == null) {
        	convertView = inflater.inflate(R.layout.item_child, parent,false);
        }
 
        TextView textView = (TextView) convertView.findViewById(R.id.list_item_text_child);
        textView.setText(mParent.get(groupPosition).getArrayChildren().get(childPosition));
 
        convertView.setTag(holder);
 
        return convertView;
    }
 
    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
 
    @Override
    public void registerDataSetObserver(DataSetObserver observer) {
        super.registerDataSetObserver(observer);
    }
 

	protected class ViewHolder {
		protected int childPosition;
		protected int groupPosition;
		protected Button button;
	}

}
