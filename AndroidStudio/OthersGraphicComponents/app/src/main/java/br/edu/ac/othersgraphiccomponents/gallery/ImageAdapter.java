package br.edu.ac.othersgraphiccomponents.gallery;


import br.edu.ac.othersgraphiccomponents.R;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;

public class ImageAdapter extends BaseAdapter {

	int mGalleryItemBackground;
    private Context mContext;

    public Integer[] mImageIds = {
            R.drawable.chaves_sozinho,
            R.drawable.chiquinha,
            R.drawable.dona_clotilde,
            R.drawable.dona_florinda,
            R.drawable.professor_jirafales,
            R.drawable.quico,
            R.drawable.seu_barriga,
            R.drawable.seu_madruga
    };

    public ImageAdapter(Context c) {
        mContext = c;
    }

    public int getCount() {
        return mImageIds.length;
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView i = new ImageView(mContext);

        i.setImageResource(mImageIds[position]);
        i.setLayoutParams(new Gallery.LayoutParams(150, 100));
        i.setScaleType(ImageView.ScaleType.FIT_XY);
        i.setBackgroundResource(mGalleryItemBackground);

        return i;
    }

	
}
