package br.edu.ac.othersgraphiccomponents;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import br.edu.ac.othersgraphiccomponents.expandablelist.ExpandableListActivity;
import br.edu.ac.othersgraphiccomponents.gallery.GalleryActivity;
import br.edu.ac.othersgraphiccomponents.viewpager.ViewPagerActivity;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	public void showGallery(View v) {
		Intent i = new Intent(this, GalleryActivity.class);
		startActivity(i);
	}
	
	public void showExpandableList(View v){
		Intent i = new Intent(this, ExpandableListActivity.class);
		startActivity(i);
	}
	
	public void showViewPager(View v){
		Intent i = new Intent(this, ViewPagerActivity.class);
		startActivity(i);
	}
}
