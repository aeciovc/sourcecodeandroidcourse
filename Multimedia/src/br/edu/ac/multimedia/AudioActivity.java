package br.edu.ac.multimedia;

import android.app.Activity;
import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;

public class AudioActivity extends Activity {

	MediaPlayer resourcePlayer;
	MediaPlayer filePlayer;
	MediaPlayer urlPlayer;
	MediaPlayer contentPlayer;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_audio);
		
		Context appContext = getApplicationContext();
		resourcePlayer = MediaPlayer.create(appContext,R.raw.groove_bom);
		
		filePlayer = MediaPlayer.create(appContext,	Uri.parse("file:///sdcard/localfile.mp3"));
		
		urlPlayer = MediaPlayer.create(appContext,Uri.parse("http://site.com/audio/audio.mp3"));
		
		contentPlayer = MediaPlayer.create(appContext, Settings.System.DEFAULT_RINGTONE_URI);
		
		
	}


	public void play(View v) {
		resourcePlayer.start();
	}
	
	public void stop(View v) {
		resourcePlayer.stop();
	}
	
	public void resume(View v) {
		resourcePlayer.seekTo(resourcePlayer.getCurrentPosition());
		resourcePlayer.start();
	}
	
	public void pause(View v) {
		resourcePlayer.pause();
	}

}
