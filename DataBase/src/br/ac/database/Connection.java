package br.ac.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class Connection {

	private static SQLiteDatabase db;
	private static SQLiteAdapter dbHelper;

	public static SQLiteDatabase getConnection(Context context) {

		if (db == null || !db.isOpen()) {
			dbHelper = new SQLiteAdapter(context);
			db = dbHelper.getWritableDatabase();
		}

		return db;
	}

	public static void close() {
		if (dbHelper != null) {
			dbHelper.close();
			// db.close();
		}
		if (db != null) {
			db.close();
		}
	}

}
