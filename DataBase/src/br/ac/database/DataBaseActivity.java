package br.ac.database;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class DataBaseActivity extends Activity {
	TextView txtCadastrados;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		txtCadastrados = (TextView) findViewById(R.id.txt_cadastrados);

		ContentValues values = new ContentValues();
		values.put("name", "Jo�o Vieira");
		values.put("number", "1678-3444");

		Connection.getConnection(this).insert("contato", null, values);

		Toast.makeText(this, "Contato Cadastrado!", Toast.LENGTH_LONG).show();

	}

	public void refresh(View v) {
		Cursor c = Connection.getConnection(this).query("contato",
				new String[] { "_id", "name", "number" }, null, null, null,
				null, "name");

		String txt_values = "";

		if (c != null) {
			while (c.moveToNext()) {
				String _id = c.getString(c.getColumnIndex("_id"));
				String name = c.getString(c.getColumnIndex("name"));
				String number = c.getString(c.getColumnIndex("number"));
				txt_values = txt_values + _id + " - " + name + " - " + number
						+ "\n";
			}
			c.close();
		}

		txtCadastrados.setText(txt_values);
	}

	public void update(View v) {

		ContentValues values = new ContentValues();
		values.put("name", "Outro Nome");
		values.put("number", "Outro Numero");
		Connection.getConnection(this).update("contato", values, "_id = 1",
				null);
		
		Toast.makeText(this, "Contato Atualizado!", Toast.LENGTH_LONG).show();
	}

	public void delete(View v) {
		Connection.getConnection(this).delete("contato", "_id = 1", null);
		Toast.makeText(this, "Contato Deletado!", Toast.LENGTH_LONG).show();
	}
}