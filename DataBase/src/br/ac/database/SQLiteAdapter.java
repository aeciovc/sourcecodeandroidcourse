package br.ac.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class SQLiteAdapter extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "my_data_base.data";
	private static final int VERSION = 1;
	
	static final String TABLE_CONTACT = "contato";
	static final String ID="_id";
	static final String NAME="name";
	static final String NUMBER="number";
	
	
	public SQLiteAdapter(Context context){
		super(context, DATABASE_NAME, null, VERSION);
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL(" CREATE TABLE "+ TABLE_CONTACT + " (_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
													 "  name TEXT," +
													 "  number TEXT );");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		
	}

}
