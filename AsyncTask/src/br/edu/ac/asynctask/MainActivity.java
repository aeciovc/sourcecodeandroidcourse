package br.edu.ac.asynctask;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;


public class MainActivity extends Activity {

	private TextView txt;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		txt = (TextView) findViewById(R.id.text);
	}
	
	public void loadContent(View v){
		
		ArrayList<String> arrayList = new ArrayList<String>();
		arrayList.add("Globo");
		arrayList.add("GNT");
		arrayList.add("SBT");
		arrayList.add("Record");
		
		new LongTask(this).execute(arrayList, txt);
		
	}



}
