package br.ac.mybrowser;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

public class MyBrowserActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        WebView viewer = (WebView) findViewById(R.id.browser);
        viewer.loadUrl("http://www.aeciocosta.com.br");
    }
}