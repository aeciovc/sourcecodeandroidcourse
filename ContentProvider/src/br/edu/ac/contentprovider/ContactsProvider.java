package br.edu.ac.contentprovider;

import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.Contacts;

public class ContactsProvider {
	
/*	// Sets the columns to retrieve for the user profile
	String[] mProjection = new String[]
	    {
	        Profile._ID,
	        Profile.DISPLAY_NAME_PRIMARY,
	        Profile.LOOKUP_KEY,
	        Profile.PHOTO_THUMBNAIL_URI
	    };*/
	
	static String[] columns = new String[] {
            Phone.DISPLAY_NAME, Phone.NUMBER, Phone.CONTACT_ID
    };
	
	
	static String[] projection = new String[] { ContactsContract.Contacts._ID,
	        ContactsContract.Contacts.DISPLAY_NAME };

	
	public static ArrayList<String> getContacts(Context mContext){
		
		ArrayList<String> contacts = new ArrayList<String>();
		
		//Uri ----
		Uri uri = ContactsContract.Contacts.CONTENT_URI;
		
		String incomingNumber = "1234567890";
		Uri lookupUri =	Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, incomingNumber);
		
		//----Filter----

		String orderBy = Contacts.DISPLAY_NAME + " ASC";
		
		String where = ContactsContract.Contacts._ID + " =  1";
		
		
		 Cursor cursor = mContext.getContentResolver()
	                .query(lookupUri, projection, null, null, null);
		 
         if (cursor != null) {
             while (cursor.moveToNext()) {
            	// String name = cursor.getString(cursor.getColumnIndex(Phone.CONTACT_ID)) + "-"+ cursor.getString(cursor.getColumnIndex(Phone.DISPLAY_NAME));
            	 String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID)) + "-"+ cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
            	 contacts.add(name);
            	 
             }
         }
         
         return contacts;
		
	}

}
