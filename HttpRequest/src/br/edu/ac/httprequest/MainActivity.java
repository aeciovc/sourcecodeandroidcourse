package br.edu.ac.httprequest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends Activity {

	TextView textResultado;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		textResultado = (TextView) findViewById(R.id.txt_resultado);
		
		Button buttonRequest = (Button) findViewById(R.id.btn_simple_request);
		buttonRequest.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				
				new SimpleRequest().execute();
			}
		});
			

		Button somaButton = (Button) findViewById(R.id.btn_soma);
		somaButton.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				EditText textNumero1 = (EditText) findViewById(R.id.edt_num1);
				EditText textNumero2 = (EditText) findViewById(R.id.edt_num2);
				
				int numero1 = Integer.parseInt(textNumero1.getText().toString());
				int numero2 = Integer.parseInt(textNumero2.getText().toString());
				
				new SomaTaskPost(MainActivity.this).execute(numero1, numero2);
			}
		});
	}

	private String requestJsonPost(long n1, long n2) {
		String resultado = null;
		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost("http://192.168.0.103:8080/HttpServerAndroid/SomaServlet");

		try {
			
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("numero1", n1);
			jsonObject.put("numero2", n2);
			
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("json", jsonObject.toString()));
			post.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			HttpResponse response = client.execute(post);
			
			String respostaJson = EntityUtils.toString(response.getEntity(), HTTP.UTF_8);
			
			JSONObject jsonResultado = new JSONObject(respostaJson);
			resultado = jsonResultado.getString("soma");

		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return resultado;
	}
	
	private String simpleRequest() {

		HttpClient client = new DefaultHttpClient();
		HttpGet request = new HttpGet("http://192.168.0.103:8080/HttpServerAndroid/SomaServlet");

		try {

			HttpResponse response = client.execute(request);
			String responseString = EntityUtils.toString(response.getEntity(), HTTP.UTF_8);

			return responseString;

		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	//Simple Request GET
	class SimpleRequest extends AsyncTask<Void, Void, String> {
		
		@Override
		protected String doInBackground(Void... params) {

			return simpleRequest();
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			textResultado.setText("Resultado: "+ result);
		}
	}
	
	
	
	//Request Soma com Json Post
	class SomaTaskPost extends AsyncTask<Integer, String, Integer> {

		private ProgressDialog progress;
		private Context context;

		public SomaTaskPost(Context context) {
			this.context = context;
		}

		@Override
		protected void onPreExecute() {
			// Cria novo um ProgressDialog e exibe
			progress = new ProgressDialog(context);
			progress.setMessage("Aguarde...");
			progress.show();
		}

		@Override
		protected Integer doInBackground(Integer... params) {

			int value1 = params[0];
			int value2 = params[1];

			int result = Integer.parseInt(requestJsonPost(value1, value2));

			return result;

		}

		@Override
		protected void onPostExecute(Integer result) {
			super.onPostExecute(result);
			progress.dismiss();
			textResultado.setText("Resultado: "+ result);
		}
		
	}

}

